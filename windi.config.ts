import { defineConfig } from 'windicss/helpers'

export default defineConfig({
    attributify: true,
    shortcuts: {
        'title': 'text-md text-green-500 block font-bold mb-2',
        'note': 'mt-1 bg-gradient-to-br from-gray-200 to-white text-xs text-gray-500 py-1 px-2',
        'warning': 'mt-1 bg-gradient-to-br from-orange-500 to-red-500 text-xs text-white py-1 px-2',
        'dashed': 'border-white border-dashed border-1',
        'input': 'appearance-none block w-full py-3 px-4 leading-tight text-green-700 bg-gray-50 focus:bg-white border border-green-200 focus:border-green-500 rounded focus:outline-none',
        'option-container': 'p-4',
        'option': 'text-md text-green-500  font-semibold mb-2 ml-2 mr-6'
    }
})