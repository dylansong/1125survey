import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import WindiCSS from 'vite-plugin-windicss'

// https://vitejs.dev/config/
export default defineConfig({
  server:{
    host: '192.168.86.39'
  },
  plugins: [react(),WindiCSS(),]
})
