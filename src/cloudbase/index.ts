import cloudbase from "@cloudbase/js-sdk";

const app = cloudbase.init({
  env: "cloudbase-prepaid-4e9ttf6aa294f5",
});

export const db = app.database();

export const auth = app.auth({ persistence: "local" });
export const provider = auth.weixinAuthProvider({
  appid: "wx2e508cc87f1f0fac",
  scope: "snsapi_base",
});

export const collection = db.collection("1125survey");
