import { useEffect, useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Header } from "./components/header";
import { BaseSurvey } from "./components/baseSurvey";
import { MoreSurvey } from "./components/moreSurvey";
import { useAtom } from "jotai";
import { willAttendAtom } from "./state";
import { useForm } from "react-hook-form";
import { For } from "windicss/types/lang/tokens";
import { auth, collection, provider } from "./cloudbase";

export type FormData = {
  isAttend: string;
  department: string;
  position: string;
  name: string;
  gender: string;
  employeeNumber: string;
  mobile: number;
  id: string;
  notes: string;
  needRoom: string;
  roomType: string;
  enterDate: string;
  leaveDate: string;
};

function App() {
  const [willAttend, setWillAttend] = useAtom(willAttendAtom);
  const {
    register,
    handleSubmit,
    watch,
    formState: { isValid, errors },
  } = useForm<FormData>();
  const watchAttend = watch("isAttend");

  async function login() {
    // 1. 建议登录前先判断当前是否已经登录
    const loginState = await auth.getLoginState();
    if (!loginState) {
      // 2. 调用微信登录API
      provider.getRedirectResult().then((loginState) => {
        if (loginState) {
          // 登录成功，本地已存在登录态
        } else {
          // 未登录，唤起微信登录
          provider.signInWithRedirect();
        }
      });
    }
  }
  useEffect(() => {
    login();
  }, []);

  const onSubmit = async (data: FormData) => {
    console.log("isvalid", isValid);
    console.log(data);
    const res = await collection.add(data);
    console.log(res.message);
  };
  return (
    <div>
      <div className="container mx-auto px-4">
        <Header />
        {Object.keys(errors).length != 0 && (
          <div className="fixed bottom-0 left-0 bg-gradient-to-tr from-red-500 to-orange-600 text-white py-2 text-center block w-screen bg-opacity-80">
            <div>
              {errors.isAttend?.type === "required" && "是否参加为必填项"}
            </div>
            <div>
              {errors.department?.type === "required" && "部门为必填项"}
            </div>
            <div>{errors.position?.type === "required" && "职位为必填项"}</div>
            <div>{errors.name?.type === "required" && "姓名为必填项"}</div>
            <div>{errors.gender?.type === "required" && "性别为必填项"}</div>
            <div>
              {errors.employeeNumber?.type === "required" && "员工号为必填项"}
            </div>
            <div>
              {errors.mobile?.type === "required" && "手机号码为必填项"}
            </div>
            <div>
              {errors.mobile?.type === "minLength" && "手机号码必须为11位"}
            </div>
            <div>
              {errors.mobile?.type === "maxLength" && "手机号码必须为11位"}
            </div>
            <div>
              {errors.id?.type === "required" &&
                "身份证号码或者护照号码为必填项"}
            </div>
            <div>{errors.notes?.type === "required" && "备注为必填项"}</div>
            <div>
              {errors.needRoom?.type === "required" && "是否需要房间为必填项"}
            </div>
            <div>
              {errors.roomType?.type === "required" && "房间类型为必填项"}
            </div>
            <div>
              {errors.enterDate?.type === "required" && "入住日期为必填项"}
            </div>
            <div>
              {errors.leaveDate?.type === "required" && "离店日期为必填项"}
            </div>
          </div>
        )}

        <form onSubmit={handleSubmit(onSubmit)}>
          <BaseSurvey register={register} />
          {watchAttend === "attend" && <MoreSurvey register={register} />}
          <div className="mx-auto w-1/2 text-center">
            <button
              type="submit"
              className="bg-gradient-to-tr from-green-500 to-blue-500 text-white font-extrabold text-xl py-1 px-4 tracking-widest my-4"
            >
              提交
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default App;
