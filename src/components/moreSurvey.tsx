import { UseFormRegister } from "react-hook-form/dist/types/form";

type props = {
  register: UseFormRegister<any>;
};

export const MoreSurvey = ({ register }: props) => {
  return (
    <div>
      <div className="mb-6">
        <label className="title" htmlFor="">
          7. 手机号
        </label>
        <input
          className="input"
          type="number"
          placeholder=""
          {...register("mobile", {
            required: true,
            maxLength: 11,
            minLength: 11,
          })}
        />
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          8. 身份证/护照号码
        </label>
        <input
          className="input"
          type="text"
          placeholder=""
          {...register("id", { required: true })}
        />
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          9. 备注 <span className="note dashed">*饮食忌口/其他</span>
        </label>
        <textarea
          className="input"
          placeholder=""
          {...register("notes", { required: true })}
        />
      </div>

      <div className="mb-6 mt-6">
        <label className="title" htmlFor="">
          10. 是否需要房间
        </label>
        <div className="option-container">
          <label className="mr-2" htmlFor="">
            <input
              type="radio"
              value="need"
              {...register("needRoom", { required: true })}
            />
            <span className="option">是</span>
          </label>
          <label htmlFor="">
            <input
              type="radio"
              value="does not need"
              {...register("needRoom", { required: true })}
            />
            <span className="option">否</span>
          </label>
        </div>
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          11. 房型
        </label>
        <div className="option-container">
          <label htmlFor="">
            <input
              type="radio"
              value="single bed"
              {...register("roomType", { required: true })}
            />
            <span className="option">大床</span>
          </label>

          <label htmlFor="">
            <input
              type="radio"
              value="double bed"
              {...register("roomType", { required: true })}
            />
            <span className="option">双床</span>
          </label>
          <div className="warning dashed mt-4">
            如是双床，需填写一份完整的个人信息。2个入住人，只需一人提交即可，无需重复填写
          </div>
        </div>
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          10. 酒店入住日期
        </label>
        <input
          className="input"
          type="date"
          placeholder="请选择酒店入住日期"
          min="2021-12-07"
          max="2021-12-31"
          {...register("enterDate", { required: true })}
        />
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          11. 酒店离开日期
        </label>
        <input
          className="input"
          type="date"
          placeholder="请选择酒店离开日期"
          min="2021-12-08"
          max="2021-12-31"
          {...register("leaveDate", { required: true })}
        />
      </div>
    </div>
  );
};
