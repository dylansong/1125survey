import { useAtom } from "jotai";
import { useEffect } from "react";
import { willAttendAtom } from "../state";
import {
  UseFormRegister,
  UseFormRegisterReturn,
} from "react-hook-form/dist/types/form";
import { FieldValues } from "react-hook-form";

type props = {
  register: UseFormRegister<any>;
};

export const BaseSurvey = ({ register }: props) => {
  const [willAttend, setWillAttend] = useAtom(willAttendAtom);
  useEffect(() => {
    console.log(willAttend);
  }, [willAttend]);
  return (
    <div>
      <div className="mb-6 mt-6">
        <label className="title" htmlFor="">
          1. 是否能拨冗参加
        </label>
        <div className="option-container">
          <label className="mr-2" htmlFor="">
            <input
              type="radio"
              value="attend"
              {...register("isAttend", { required: true })}
            />
            <span className="option">是</span>
          </label>
          <label htmlFor="">
            <input
              type="radio"
              value="absent"
              {...register("isAttend", { required: true })}
              // onChange={() => setWillAttend(false)}
              // onChange={(e) => setWillAttend(!e.target.checked)}
            />
            <span className="option">否</span>
          </label>
        </div>
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          2.部门
        </label>
        <input
          className="input"
          type="text"
          placeholder=""
          {...register("department", { required: true })}
        />
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          3. 职位
        </label>
        <input
          className="input"
          type="text"
          placeholder=""
          {...register("position", { required: true })}
        />
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          4. 姓名
        </label>
        <input
          className="input"
          type="text"
          placeholder=""
          {...register("name", { required: true })}
        />
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          5. 性别
        </label>
        <div className="option-container">
          <label className="mr-2" htmlFor="">
            <input
              type="radio"
              value="male"
              {...register("gender", { required: true })}
              // onChange={(e) => setWillAttend(e.target.checked)}
            />
            <span className="option">男</span>
          </label>
          <label htmlFor="">
            <input
              type="radio"
              value="female"
              {...register("gender", { required: true })}
              // onChange={(e) => setWillAttend(!e.target.checked)}
            />
            <span className="option">女</span>
          </label>
        </div>
      </div>

      <div className="mb-6">
        <label className="title" htmlFor="">
          6. 员工号
        </label>
        <input
          className="input"
          type="text"
          placeholder=""
          {...register("employeeNumber", { required: true })}
        />
      </div>
    </div>
  );
};
