export const Header = () => {
  return (
    <div>
      <nav className="flex justify-center p-4">
        <span className=" font-extrabold font-heading text-3xl bg-gradient-to-br from-green-500 to-blue-500 bg-clip-text text-transparent ">
          VIP行程信息收集
        </span>
      </nav>
      <p className="mb-2 leading-snug   px-4 py-2 text-bold text-sm warning">
        温馨提示“请在12月8日 之前填写，将以此前最后一次的提交信息为准，预留房间
      </p>
    </div>
  );
};
